//
// Created by zhouheyu on 18-7-16.
//

#ifndef DISKSIM_FTL_FTL_SCHEME_H
#define DISKSIM_FTL_FTL_SCHEME_H

#include "ssd_interface.h"

//和预取策略相关的定义
#define THRESHOLD 2
#define NUM_ENTRIES_PER_TIME 8

/***********************************************************************
  author  :zhou jie
 ***********************************************************************/
int update_flag;
//shzb:用于判断连续缓存中的部分映射项更新标志是否置零
//翻译页的统计读写次数
int translation_read_num ;
int translation_write_num;

// 预取相关的全局变量
int sequential_count;
int big_request_entry ;
int big_request_count ;


/***************新的预热函数*************************/
void FTL_Warm(int *pageno,int *req_size,int operation);

/***********************************************************************
 *    author: zhoujie
 *  封装 callFsim的代码函数
 ***********************************************************************/
void SecnoToPageno(int secno,int scount,int *blkno,int *bcount,int flash_flag);
/***********************************************************************
 *  author :zhoujie
 * SDFTL 代码执行的内部封装函数
 ***********************************************************************/
//void Hit_CMT_Entry(int blkno,int operation);
//void CMT_Is_Full();
//void Hit_SCMT_Entry(int blkno,int operation);
//void Hit_SL_CMT_Entry(int blkno,int operation);
//void pre_load_entry_into_SCMT(int *pageno,int *req_size,int operation);
//void req_Entry_Miss_SDFTL(int blkno,int operation);
//void SDFTL_Scheme(int *pageno,int *req_size,int operation,int flash_flag);


/***********************************************************************
 *             author:zhoujie       DFTL  主函数逻辑实现
 ***********************************************************************/
void DFTL_Scheme(int *pageno,int *req_size,int operation,int flash_flag);
/********************************************************
 *         author:zhoujie     DFTL 封装的相关函数
 * *******************************************************/
//void DFTL_init_arr();
//void DFTL_Ghost_CMT_Full();
//void DFTL_Real_CMT_Full();
//void DFTL_Hit_Ghost_CMT(int blkno);
//void DFTL_Hit_Real_CMT(int blkno);


#endif //DISKSIM_FTL_FTL_SCHEME_H
