//
// Created by zhouheyu on 18-7-16.
//

#include "FTL_Scheme.h"
int update_flag=0;
int translation_read_num = 0;
int translation_write_num = 0;

// 预取相关的全局变量
int sequential_count=0;
int big_request_entry=0 ;
int big_request_count=0;

//从ssd_interface.c中交互的全局变量
extern int page_num_for_2nd_map_table;
extern int rqst_cnt;

/************************CallFsim预处理函数**************************/
void SecnoToPageno(int secno,int scount,int *blkno,int *bcount,int flash_flag)
{
    switch(ftl_type){
        case 1:
            // page based FTL
            *blkno = secno / 4;
            *bcount = (secno + scount -1)/4 - (secno)/4 + 1;
            break;
        case 2:
            // block based FTL
            *blkno = secno/4;
            *bcount = (secno + scount -1)/4 - (secno)/4 + 1;
            break;
        case 3:
            // o-pagemap scheme
            if(flash_flag==0){
                *blkno = secno / 4;
                *bcount = (secno + scount -1)/4 - (secno)/4 + 1;
                //  CFTL的SLC只是简单的循环循环队列在这没有做统计
                //  SLC_write_page_count+=(*bcount);
            }
            else{
                *blkno = secno / 8;
                *blkno += page_num_for_2nd_map_table;
                *bcount = (secno + scount -1)/8 - (secno)/8 + 1;
            }
            break;
        case 4:
            // FAST scheme
            *blkno = secno/4;
            *bcount = (secno + scount -1)/4 - (secno)/4 + 1;
            break;
    }
}


void FTL_Warm(int *pageno,int *req_size,int operation)
{
//    首先是翻译页的读取,不经过CMT，直接进行预热
    int blkno=(*pageno),cnt=(*req_size);
    if(operation==0){
        send_flash_request(((blkno-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4,4,1,2);
        translation_read_num++;
        send_flash_request(((blkno-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4,4,0,2);
        translation_write_num++;
    } else{
        send_flash_request(((blkno-page_num_for_2nd_map_table)/MAP_ENTRIES_PER_PAGE)*4,4,1,2);
        translation_read_num++;
    }


//    其次数据页的读取
    sequential_count = 0;
    //之后连续的请求因为映射加载完成直接读取写入操作
    for(;(cnt>0)&&(sequential_count<NUM_ENTRIES_PER_TIME);cnt--)
    {
        if(operation==0)
        {
            write_count++;
        }
        else
            read_count++;
        send_flash_request(blkno*4,4,operation,1);
        blkno++;
        rqst_cnt++;
        sequential_count++;
    }

    //zhoujie
    *req_size=cnt;
    *pageno=blkno;

}
